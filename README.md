# CesiumWebsite

## Documentation en français

* [Restrictions à votre liberté d'expression](doc/fr/restrictions-liberte-d-expression.md)
* [Comment tester installer le site sur son ordinateur ?](doc/fr/installation.md)
* [Comment traduire le site](doc/fr/comment-traduire-le-site.md)
* [Comment mettre à jour les liens de téléchargement au changement de version](doc/fr/comment-mettre-a-jour-les-liens-de-telechargement.md)
* [Licences](docs/fr/licences.md)

## Documentation in English

* [How to install Cesium website on your computer](doc/en/install.md)
* [How to translate the website](doc/en/how-to-translate-the-website.md)
* [Licences](doc/en/licences.md)


